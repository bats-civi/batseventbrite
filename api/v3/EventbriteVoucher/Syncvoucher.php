<?php

use CRM_Batseventbrite_ExtensionUtil as E;

const CLASS_CODE_FIELD = 'custom_4';
const EB_EVENT_ID_FIELD = 'custom_5';
const CODE_FIELD = 'custom_157';
const EB_DISCOUNT_ID_FIELD = 'custom_158';
const LIMIT_TO_CLASS_CODE_FIELD = 'custom_159';
const LIMIT_TO_CLASS_ID_FIELD = 'custom_202';
const APPLIED_TO_EVENT_ID_FIELD = 'custom_411';
const APPLIED_TO_CLASS_STATUS_FIELD = 'custom_410';
const DISCOUNT_REASON_FIELD = 'custom_160';
const CREDIT_AMOUNT_FIELD = 'custom_161';
const PERCENT_OFF_FIELD = 'custom_162';
const EB_DISCOUNT_STATUS_FIELD = 'custom_163';
const EB_ORDER_ID_FIELD = 'custom_413';
const CONTRIBUTION_ID_FIELD = 'custom_171';
const EXPIRES_ON_FIELD = 'custom_172';
const ORIGIN_ID_FIELD = 'custom_173';
const ORIGINAL_VALUE_FIELD = 'custom_175';
const USED_ON_DATE_FIELD = 'custom_175';
const ERROR_MESSAGE_FIELD = 'custom_176';
const IS_ACCESS_CODE_FIELD = 'custom_203';
const APPLIED_TO_CLASS_NAME = 'custom_169';
const APPLIED_TO_CLASS_CODE = 'custom_170';
const APPLIED_TO_CLASS_TITLE = 'custom_412';
const APPLIED_TO_CLASS_START_DATE = 'custom_414';

const STATUS_NEW = 1;
const STATUS_READY = 2;
const STATUS_USED = 3;
const STATUS_ERROR = 4;
const STATUS_EXPIRED = 5;
const STATUS_IMPORTED = 6;

const CLASS_PENDING = 1;
const CLASS_STARTED = 2;
const CLASS_CANCELLED = 3;

function getRandomString($n) {
  $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
  $randomString = '';

  for ($i = 0; $i < $n; $i++) {
    $index = rand(0, strlen($characters) - 1);
    $randomString .= $characters[$index];
  }

  return $randomString;
}

function adjustActivity(&$newVoucher) {
  if (is_null($newVoucher[CODE_FIELD])) {
    // set a random code
    $newVoucher[CODE_FIELD] = getRandomString(6);
  }
  if ($newVoucher[CREDIT_AMOUNT_FIELD] > 0 AND $newVoucher[ORIGINAL_VALUE_FIELD] = 0) {
    $newVoucher[ORIGINAL_VALUE_FIELD] = $newVoucher[CREDIT_AMOUNT_FIELD];
  }
}

function generateDiscountParams($newVoucher) {
  $discountParams = array();
  $discountParams['code'] = $newVoucher[CODE_FIELD];
  $discountParams['type'] = 'coded';
  $discountParams['quantity_available'] = 1;

  if ($newVoucher[CREDIT_AMOUNT_FIELD] > 0) {
    $discountParams['amount_off'] = $newVoucher[CREDIT_AMOUNT_FIELD];
  } else {
    $discountParams['percent_off'] = $newVoucher[PERCENT_OFF_FIELD];
  }
  if (!is_null($newVoucher[LIMIT_TO_CLASS_ID_FIELD])) {
    $civiEventId = $newVoucher[LIMIT_TO_CLASS_ID_FIELD];
    \CRM_Core_Error::debug_var("limiting to event id", $civiEventId);
    $result = civicrm_api3('Event', 'get', [
      'sequential' => 1,
      'id' => $civiEventId
    ]);
    if ($result['count'] == 1) {
      $discountParams['event_id'] = $result['values'][0][EB_EVENT_ID_FIELD];

      if ($newVoucher[IS_ACCESS_CODE_FIELD]) {
        $discountParams['type'] = 'access';
      }
    }

  }
  return $discountParams;
}

function checkUsedDiscounts() {
  $activities = [];
  \CRM_Core_Error::debug_log_message("in checkUsedDiscounts");

  // if class status is null, make sure we have the Event ID and set status to Pending (safe default)
  $result = civicrm_api3('Activity', 'get', [
    'sequential' => 1,
    'activity_type_id' => "Discount Code/Credit",
    EB_DISCOUNT_STATUS_FIELD => ['IN' => [STATUS_USED]],
    APPLIED_TO_CLASS_STATUS_FIELD => ["IS NULL" => 1]
  ]);
  foreach ($result['values'] as $id=>$discountActivity) {
    \CRM_Core_Error::debug_var("processing used voucher with null class status", $discountActivity);

    // most likely we have the EB Order ID
    $ebOrderID = $discountActivity[EB_ORDER_ID_FIELD];
    \CRM_Core_Error::debug_var("order id", $ebOrderID);

    if ($ebOrderID === null) {
      \CRM_Core_Error::debug_log_message("no order ID, nothing we can do, skipping for now");
      continue;
    }

    try {
      $eb = CRM_Eventbrite_EventbriteApi::singleton();
      $ebResp = $eb->request("orders/$ebOrderID");
    } catch (Exception $e) {
      $errorMessage = "EB API Error: {$e->getMessage()}";
      \CRM_Core_Error::debug_log_message($errorMessage);
      continue;
    }

    \CRM_Core_Error::debug_var("ebResp", $ebResp);
    \CRM_Core_Error::debug_log_message("About to fetch event id from resp...");
    $ebEventID = $ebResp['event_id'];
    \CRM_Core_Error::debug_var("eb event id", $ebEventID);

    $updateParams = [
      'id' => $discountActivity['id'],
      APPLIED_TO_EVENT_ID_FIELD => $ebEventID,
      APPLIED_TO_CLASS_STATUS_FIELD => CLASS_PENDING
    ];
    \CRM_Core_Error::debug_var("update params", $updateParams);
    $updateResult = civicrm_api3('Activity', 'create', $updateParams);
    \CRM_Core_Error::debug_var("update result", $updateResult);
    $activities[] = $discountActivity['id'];
  }

  // process Pending classes
  $result = civicrm_api3('Activity', 'get', [
    'sequential' => 1,
    'activity_type_id' => "Discount Code/Credit",
    EB_DISCOUNT_STATUS_FIELD => STATUS_USED,
    APPLIED_TO_CLASS_STATUS_FIELD => CLASS_PENDING
  ]);
  foreach ($result['values'] as $id=>$discountActivity) {
    \CRM_Core_Error::debug_var("processing voucher", $discountActivity);

    $ebEventID = $discountActivity[APPLIED_TO_EVENT_ID_FIELD];
    if ($ebEventID === NULL or $ebEventID == 0) {
      \CRM_Core_Error::debug_log_message("no event ID for this voucher, skipping");
      continue;
    }
    \CRM_Core_Error::debug_var("eventbrite id", $ebEventID);

    $result = civicrm_api3('Event', 'get', [
      'sequential' => 1,
      'custom_5' => $ebEventID
    ]);
    \CRM_Core_Error::debug_var("result of event search", $result);
    $civiEvent = $result['values'][0];
    \CRM_Core_Error::debug_var("corresponding event", $civiEvent);

    if ($civiEvent === null) {
      continue;
    }
    $classStartDate = $civiEvent['start_date'];
    $isClassCancelled = $civiEvent['custom_154'];
    $isClassStarted = ($civiEvent['start_date'] < date('Y-m-d H:i:s'));

    \CRM_Core_Error::debug_var("is class cancelled", $isClassCancelled);
    \CRM_Core_Error::debug_var("is class started", $isClassStarted);
    \CRM_Core_Error::debug_var("class start date", $classStartDate);

    if ($isClassCancelled) {
      $classStatus = CLASS_CANCELLED;
    } else {
      if ($isClassStarted) {
        $classStatus = CLASS_STARTED;
      } else {
        $classStatus = CLASS_PENDING;
      }
    }
    \CRM_Core_Error::debug_var("class status", $classStatus);

    $activityUpdateParams = [
      'id' => $discountActivity['id'],
    ];

    if ($discountActivity['subject'] === "" OR $discountActivity['subject'] === NULL) {
      if ($discountActivity[CREDIT_AMOUNT_FIELD] > 0) {
        $activityUpdateParams['subject'] = "Eventbrite Discount Code " . $discountActivity[CODE_FIELD] . " (" . $discountActivity[CREDIT_AMOUNT_FIELD] . ")";
      } else {
        $activityUpdateParams['subject'] = "Eventbrite Discount Code " . $discountActivity[CODE_FIELD] . " (" . $discountActivity[PERCENT_OFF_FIELD] . "%)";
      }
    }

    if ($discountActivity[APPLIED_TO_CLASS_NAME] === NULL AND $civiEvent['custom_3'] != NULL) {
      $result = civicrm_api3('OptionValue', 'getvalue', [
        'return' => "label",
        'option_group_id' => "class_name_20200615184556",
        'value' => $civiEvent['custom_3']
      ]);
      \CRM_Core_Error::debug_var("result", $result);
      $activityUpdateParams[APPLIED_TO_CLASS_NAME] = $result;
    }

    if ($discountActivity[APPLIED_TO_CLASS_CODE] === NULL AND $civiEvent['custom_4'] != NULL) {
      $activityUpdateParams[APPLIED_TO_CLASS_CODE] = $civiEvent['custom_4'];
    }

    if ($discountActivity[APPLIED_TO_CLASS_TITLE] === NULL) {
      $activityUpdateParams[APPLIED_TO_CLASS_TITLE] = $civiEvent['title'];
    }

    if ($discountActivity[APPLIED_TO_CLASS_START_DATE] === NULL) {
      $activityUpdateParams[APPLIED_TO_CLASS_START_DATE] = $classStartDate;
    }

    if ($classStatus != CLASS_PENDING) {
      $activityUpdateParams[APPLIED_TO_CLASS_STATUS_FIELD] = $classStatus;
    }

    \CRM_Core_Error::debug_log_message("about to update activity ");
    $updateResult = civicrm_api3('Activity', 'create', $activityUpdateParams);
    \CRM_Core_Error::debug_var("update result", $updateResult);
    $activities[] = $discountActivity['id'];
  }
  return $activities;
}

function processNewDiscounts() {
  // Try to process New discounts and any discounts with Errors
  $result = civicrm_api3('Activity', 'get', [
    'sequential' => 1,
    'activity_type_id' => "Discount Code/Credit",
    EB_DISCOUNT_STATUS_FIELD => ['IN' => [STATUS_NEW, STATUS_ERROR]],
  ]);

  $activities = [];
  foreach ($result['values'] as $id=>$newVoucher) {
    \CRM_Core_Error::debug_var("processing voucher", $newVoucher);

    $errorMessage = validateVoucher($newVoucher);
    adjustActivity($newVoucher);
    $discountParams = generateDiscountParams($newVoucher);
    \CRM_Core_Error::debug_var("discount params", $discountParams);

    if (!isset($discountParams)) {
        $errorMessage .= " discount params not set!";
    }

    if ($errorMessage == "" or is_null($errorMessage)) {
      try {
        $eb = CRM_Eventbrite_EventbriteApi::singleton();
        $ebResp = $eb->requestOrg("discounts", array('discount' => $discountParams), NULL, NULL, "POST");
        \CRM_Core_Error::debug_var("created discount, response...", $ebResp);
        $newVoucher[EB_DISCOUNT_ID_FIELD] = $ebResp['id'];
        $newVoucher[EB_DISCOUNT_STATUS_FIELD] = STATUS_READY;
      } catch (Exception $e) {
        \CRM_Core_Error::debug_var("error occurred creating discount", $e);
        $errorMessage = "EB API Error: {$e->getMessage()}";
      }
    } else {
        \CRM_Core_Error::debug_var("oerror message was set", $errorMessage);
    }

    // errorMessage may have been changed if an exception occurred creating discount
    if ($errorMessage == "" or is_null($errorMessage)) {
      if (strlen($newVoucher[ERROR_MESSAGE_FIELD]) > 0) {
        // reset error message if it's resolved now
        $newVoucher[ERROR_MESSAGE_FIELD] = "Previous error resolved.";
      }
    } else {
      \CRM_Core_Error::debug_log_message($errorMessage);
      $newVoucher[EB_DISCOUNT_STATUS_FIELD] = STATUS_ERROR;
      $newVoucher[ERROR_MESSAGE_FIELD] = $errorMessage;
    }

    \CRM_Core_Error::debug_var("voucher", $newVoucher);
    $updateResult = civicrm_api3('Activity', 'update', $newVoucher);
    \CRM_Core_Error::debug_var("update result", $updateResult);
    $activities[] = $id;
  }
  return $activities;
}

function validateVoucher($newVoucher) {
  $errorMessage = "";

  if ($newVoucher[CREDIT_AMOUNT_FIELD] > 0 AND $newVoucher[PERCENT_OFF_FIELD] > 0) {
    $errorMessage .= " Can't set both amount_off and percent_off, only set one.";
  }

  if ($newVoucher[PERCENT_OFF_FIELD] < 0 OR $newVoucher[PERCENT_OFF_FIELD] > 100) {
    $errorMessage .= " Percent off should be a number between 1 and 100";
  }

  if ($newVoucher[EB_ORDER_ID_FIELD]) {
    $errorMessage .= " Voucher already used.";
  }

  return $errorMessage;
}

function expireOldDiscounts() {
  \CRM_Core_Error::debug_log_message("in expireOldDiscounts");
  $activities = [];
  $result = civicrm_api3('Activity', 'get', [
    'sequential' => 1,
    'activity_type_id' => "Discount Code/Credit",
    EB_DISCOUNT_STATUS_FIELD => ['IN' => [STATUS_READY, STATUS_IMPORTED]],
    EXPIRES_ON_FIELD => array("<" => date("Y-m-d")),
  ]);
  foreach ($result['values'] as $voucher) {
    \CRM_Core_Error::debug_var("expiring voucher", $voucher);
    $ebDiscountId = $voucher[EB_DISCOUNT_ID_FIELD];
    $eb = CRM_Eventbrite_EventbriteApi::singleton();
    try {
      $ebResp = $eb->request("discounts/$ebDiscountId", NULL, NULL, NULL, "DELETE");
    } catch (Exception $e) {
      \CRM_Core_Error::debug_var("exception deleting voucher", $e);
      // TODO if we're here, probably should still try to amend end date?
      // TODO validate specific error message for attempting to delete used discount...
    }
    $expireResult = civicrm_api3('Activity', 'create', [
      'id' => $voucher['id'],
      EB_DISCOUNT_STATUS_FIELD => STATUS_EXPIRED
    ]);
    $activities[] = $voucher['id'];
  }
  return $activities;
}



/**
 * EventbriteVoucher.Syncvoucher API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_eventbrite_voucher_Syncvoucher_spec(&$spec) {
}

/**
 * EventbriteVoucher.Syncvoucher API
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_eventbrite_voucher_Syncvoucher($params) {
  $returnValues = array_merge(expireOldDiscounts(), processNewDiscounts(), checkUsedDiscounts());
  return civicrm_api3_create_success($returnValues, $params, 'EventbriteVoucher', 'Syncvoucher');
}
