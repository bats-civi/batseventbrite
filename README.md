# batseventbrite

Custom code for BATS Improv synchronization with Eventbrite. Relies on a modified version of com.joineryhq.eventbrite extension. (This extension can be end-of-lifed after class sales are moved to Civi in the future.)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Usage

The com.joineryhq.eventbrite extension should have added the Eventbrite.runqueue, EventbriteQueue.populateevents scheduled job and the Attendee, Event, and Order listeners should automatically respond to hooks.

The EventbriteVoucher.Syncvoucher API endpoint should be called from a scheduled job - this will upload voucher codes in Discount Code/Credit activities to Eventbrite as EB discount codes.

