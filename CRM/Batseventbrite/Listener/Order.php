<?php

const IS_CLASS_CANCELLED_FIELD = 'custom_154';
const PAYPAL_CLEARING_ACCOUNT = 17;
const PAYPAL_FIXED = 0.30;
const PAYPAL_PERCENT = 0.022;

const EVENTBRITE_DISCOUNT_ID_FIELD = 'custom_158';
const EVENTBRITE_ORDER_ID_FIELD = 'custom_413';
const DISCOUNT_CODE_FIELD = 'custom_157';
const DISCOUNT_REASON_FIELD = 'custom_160';

const SCHOLARSHIP_REASON = 105;

const PAYPAL_PAYMENT_PROCESSOR = 3;
const CREDIT_PAYMENT_PROCESSOR = 5;
const SCHOLARSHIP_PAYMENT_PROCESSOR = 7;

class CRM_Batseventbrite_Listener_Order {
  public function handleTempContactParamsAssigned($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleTempContactParamsAssigned");
    $processor = $symfonyEvent->getSubject();
    \CRM_Core_Error::debug_var("pos is", $pos);
    $pos = strpos($processor->tempContactParams['last_name'], "Jr");
    if ($pos !== false) {
      $lastNameWithoutJr = substr($processor->tempContactParams['last_name'], 0, $pos);
      \CRM_Core_Error::debug_var("trimed last name", $lastNameWithoutJr);
      $processor->tempContactParams['last_name'] = trim($lastNameWithoutJr);
    }
    \CRM_Core_Error::debug_var("temp contact params after adjust are", $processor->tempContactParams);
  }

  public function handleOrderAttendeesListSet($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleOrderAttendeesListSet");
    $processor = $symfonyEvent->getSubject();

    // assume all attendees are valid students
    foreach ($processor->order['attendees'] as $attendee) {
      $processor->orderAttendees[$attendee['id']] = $attendee;
    }
  }

  public function handleFeesSetup($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleFeesSetup");
    $processor = $symfonyEvent->getSubject();

    $processor->scholarshipSum = 0;
    $processor->creditSum = 0;
  }

  public function paypalFees($total) {
    return PAYPAL_FIXED + PAYPAL_PERCENT * $total;
  }

  public function handleProcessCurrentAttendeeFees($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleProcessCurrentAttendeeFees");
    $processor = $symfonyEvent->getSubject();
    $attendee = $processor->currentAttendeeProcessor;

    if (!isset($processor->discountActivity)) {
        $processor->discountActivity = $attendee->discountActivity;
        $processor->isCreditVoucher = $attendee->isCreditVoucher;
        \CRM_Core_Error::debug_var("copied discount activity from attendee", $processor->discountActivity);
    }

    if ($attendee->isCreditVoucher and $attendee->valueUsed > 0) {
      $processor->creditSum += $attendee->valueUsed;
      if ($processor->grossSum > 0 and $processor->grossSum == $attendee->valueUsed) {
        $processor->grossSum = 0;
      }
    }

    if (!$attendee->isCreditVoucher and $processor->feesValue == 0.00 and $processor->grossValue > 0.0) {
      $processor->feesValue = self::paypalFees($processor->grossValue);
    }

    // add credit amount to gross sum (after calculating fees)

    \CRM_Core_Error::debug_var("grossSum", $processor->grossSum);
    \CRM_Core_Error::debug_var("creditSum", $processor->creditSum);
    $processor->grossSum += $processor->creditSum;
    $processor->feesSum += $processor->feesValue;
  }

  public function handleContributionParamsAssigned($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleContributionParamsAssigned");
    $processor = $symfonyEvent->getSubject();
    $processor->isCheckPayment = false;

    $processor->contributionParams['payment_instrument_id'] = "PayPal";
    $processor->contributionParams['source'] .= " {$processor->event['custom_4']}";
  }

  public function handlePaymentParamsAssigned($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handlePaymentParamsAssigned");
    $processor = $symfonyEvent->getSubject();

    \CRM_Core_Error::debug_var("existing pmt", $processor->existingPaymentsTotalValue);

    if ($processor->existingPaymentsTotalValue > 0) {
      $processor->proposedPayments = [];
      return;
    }

    $primaryPayment = $processor->proposedPayments[0];
    $primaryPayment['payment_processor_id'] = PAYPAL_PAYMENT_PROCESSOR;

    $processor->proposedPayments = array($primaryPayment);
    \CRM_Core_Error::debug_var("proposed payments", $processor->proposedPayments);

    if ($processor->isCreditVoucher) {
      $discountReason = $processor->discountActivity[DISCOUNT_REASON_FIELD];
      \CRM_Core_Error::debug_var("discount activity is", $processor->discountActivity);
      \CRM_Core_Error::debug_var("discount reason is", $discountReason);
      if ($discountReason == SCHOLARSHIP_REASON) {
        \CRM_Core_Error::debug_log_message("foudn scholarship reason");
        $payment_processor_id = SCHOLARSHIP_PAYMENT_PROCESSOR;
      } else {
        \CRM_Core_Error::debug_log_message("credit payment reason");
        $payment_processor_id = CREDIT_PAYMENT_PROCESSOR;
      }

      $creditPayment = array(
        'contribution_id' => $processor->contribution['id'],
        'trxn_date' => CRM_Utils_Date::processDate(CRM_Utils_Array::value('created', $processor->order)),
        'payment_processor_id' => $payment_processor_id,
        'trxn_id' => $processor->discountActivity['id'],
        'check_number' => $processor->voucherCode,
        'total_amount' => $processor->creditSum,
        'fee_amount' => 0,
        'net_amount' => $processor->creditSum,
        'is_send_contribution_notification' => 0
      );

      \CRM_Core_Error::debug_var("credit", $processor->creditSum);
      \CRM_Core_Error::debug_var("gross", $processor->grossSum);
      \CRM_Core_Error::debug_var("equals", $processor->creditSum == $processor->grossSum);
      if ($processor->creditSum == $processor->grossSum) {
        $processor->proposedPayments = [$creditPayment];
      } else {
        // adjust first payment amount by credit amount
        $primaryPayment['total_amount'] -= $processor->creditSum;
        $primaryPayment['fee_amount'] = self::paypalFees($primaryPayment['total_amount']);
        $primaryPayment['net_amount'] = $primaryPayment['total_amount'] - $primaryPayment['fee_amount'];
        $processor->proposedPayments = [$primaryPayment, $creditPayment];
        \CRM_Core_Error::debug_var("proposed payments for part credit payment", $processor->proposedPayments);
      }
    }
  }
}
