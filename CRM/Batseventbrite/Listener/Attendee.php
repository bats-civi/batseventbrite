<?php

const CLASS_NAME_FIELD = 'custom_3';
const CLASS_CODE_FIELD = 'custom_4';

const STUDENT_ROLE = 7;
const CODE_FIELD = 'custom_157';
const CREDIT_AMOUNT_FIELD = 'custom_161';
const PERCENT_OFF_FIELD = 'custom_162';
const EVENTBRITE_DISCOUNT_ID_FIELD = 'custom_158';
const EB_DISCOUNT_STATUS_FIELD = 'custom_163';
const USED_IN_EVENTBRITE = 3;
const VALUE_USED_FIELD = 'custom_164';
const USED_ON_DATE_FIELD = 'custom_175';

const DISCOUNT_REASON_FIELD = 'custom_160';

const EVENTBRITE_EVENT_ID_FIELD = 'custom_411';
const EVENTBRITE_ORDER_ID_FIELD = 'custom_413';
const APPLIED_TO_CLASS_NAME_FIELD = 'custom_169';
const APPLIED_TO_CLASS_CODE_FIELD = 'custom_170';

const ORIGIN_ID_FIELD = 'custom_174';
const ORIGINAL_VALUE_FIELD = 'custom_174';

class CRM_Batseventbrite_Listener_Attendee {
  public function createOrUpdateDiscountActivity($processor, $existingActivity=null) {
    $classNameOptions = civicrm_api3('Event', 'getoptions', [
      'field' => "custom_3",
    ])['values'];
    $className = $classNameOptions[$processor->event[CLASS_NAME_FIELD]];

    $discountActivityOptions = array(
      EB_DISCOUNT_STATUS_FIELD => USED_IN_EVENTBRITE,
      VALUE_USED_FIELD => $processor->valueUsed,
      USED_ON_DATE_FIELD => CRM_Utils_Date::processDate(CRM_Utils_Array::value('created', $processor->attendee)),
      EVENTBRITE_EVENT_ID_FIELD => $processor->eventId,
      EVENTBRITE_ORDER_ID_FIELD => $processor->attendee['order_id'],
      APPLIED_TO_CLASS_NAME_FIELD => $className,
      APPLIED_TO_CLASS_CODE_FIELD => $processor->event[CLASS_CODE_FIELD],
      'status_id' => 'Completed',
    );

    if (is_null($existingActivity)) {
      // create an activity from scratch,
      // add other info we need to know...
      $discountActivityOptions['activity_type_id'] = 89;
      $discountActivityOptions[CODE_FIELD] = $processor->discountCode;
      $discountActivityOptions['source_contact_id'] = 2;
      $discountActivityOptions['target_id'] = $processor->contactId;
      $discountActivityOptions['subject'] = $processor->discountDescription;
      $discountActivityOptions[CREDIT_AMOUNT_FIELD] = $processor->amountOff;
      $discountActivityOptions[PERCENT_OFF_FIELD] = $processor->percentOff;
      $discountActivityOptions[EVENTBRITE_DISCOUNT_ID_FIELD] = $promoCode['id'];
    } else {
      // update the existing activity
      $discountActivityOptions['id'] = $existingActivity['id'];
      if (is_null($existingActivity['subject'])) {
        $discountActivityOptions['subject'] = $processor->discountDescription;
      }
    }

    \CRM_Core_Error::debug_log_message("about to creat or update activity...\n");
    \CRM_Core_Error::debug_var("discountActivityOptions", $discountActivityOptions);
    $result = _eventbrite_civicrmapi('Activity', 'create', $discountActivityOptions);
    \CRM_Core_Error::debug_var("result of create/update  activity", $result);
    $result = _eventbrite_civicrmapi('Activity', 'get', array('id' => $result['id']));
    \CRM_Core_Error::debug_var("result of get activity", $result);
    $processor->discountActivity = $result['values'][array_key_first($result['values'])];
    \CRM_Core_Error::debug_var("assigning discount activity", $processor->discountActivity);
  }

  public function processManualRegistration($processor) {
    \CRM_Core_Error::debug_log_message("in processManualRegistration");

    if (!is_null($processor->attendee['promotional_code'])) {
      \CRM_Core_Error::debug_log_message("already processed promo code, nothing more to do");
      return;
    }

    $paidPrice = $processor->attendee['costs']['gross']['major_value'];
    $eventbriteFees = $processor->attendee['costs']['payment_fee']['major_value'];
    $priceButNoFees = ($paidPrice > 0) and ($eventbriteFees == 0);

    if ($priceButNoFees) {
      $paidPrice = 0;
    }

    if ($paidPrice == 0) {
      \CRM_Core_Error::debug_log_message("search for activity...");
      // search for a discount code Activity matching the order ref
      $result = _eventbrite_civicrmapi('Activity', 'get', array(
        'activity_type_id' => 89,
        EVENTBRITE_ORDER_ID_FIELD => $processor->attendee['order_id']
      ));
      \CRM_Core_Error::debug_var("result of esearch", $result);

      // no activity found, nothing further to do
      if ($result['count'] == 0) {
        return;
      }

      $existingActivity = $result['values'][array_key_first($result['values'])];
      \CRM_Core_Error::debug_var("existing activity", $existingActivity);
      $processor->isCreditVoucher = true;
      $processor->discountDescription = "Manually Processed Order Discount";
      \CRM_Core_Error::debug_var("ticket price is ", $processor->ticketPrice);
      \CRM_Core_Error::debug_var("paid price is ", $paidPrice);
      $processor->valueUsed = $processor->ticketPrice - $paidPrice;
      \CRM_Core_Error::debug_var("value used set to", $processor->valueUsed);
      self::createOrUpdateDiscountActivity($processor, $existingActivity);
    }
  }

  public function processPromoCode($processor) {
    \CRM_Core_Error::debug_log_message("in processPromoCode");
    \CRM_Core_Error::debug_var("processor", $processor);

    $promoCode = $processor->attendee['promotional_code'];
    $processor->discountCode = $promoCode['code'];
    $processor->discountId = $promoCode['id'];
    $processor->discountDescription = "Eventbrite Discount {$processor->discountCode}";
    $paidPrice = $processor->attendee['costs']['gross']['major_value'];
    $processor->valueUsed = $processor->ticketPrice - $paidPrice;

    if (($promoCode['percent_off'] == 0) and ($promoCode['amount_off']['major_value'] == 0)) {
      // no discount to process, so nothing to do
      return;
    }

    // look for existing Discount activity to update with voucher use
    $result = _eventbrite_civicrmapi('Activity', 'get', array(
      'activity_type_id' => 89,
      EVENTBRITE_DISCOUNT_ID_FIELD => $processor->discountId
    ));

    if ($result['count'] > 0) {
      $existingActivity = $result['values'][array_key_first($result['values'])];
    } else {
      // for now, allow creating a new activity on the fly
      $existingActivity = null;
    }

    if ($existingActivity === null) {
      // TODO validate that voucher type corresponds to amount vs. percent off
      if (array_key_exists('amount_off', $promoCode) and strlen($processor->discountCode) == 6) {
        \CRM_Core_Error::debug_log_message("setting isCreditVoucher to true");
        $processor->isCreditVoucher = true;
        $discountDescription .= " ({$promoCode['amount_off']['display']})";
        $processor->amountOff = $promoCode['amount_off']['major_value'];
        $processor->percentOff = NULL;
      } else if (array_key_exists('amount_off', $promoCode)) {
        // assume non-credit but amount rather than percent off (shouldn't happen any more)
        $processor->isCreditVoucher = false;
        $processor->discountDescription .= " ({$promoCode['amount_off']['display']})";
        $processor->amountOff = $promoCode['amount_off']['major_value'];
        $processor->percentOff = NULL;
      } else {
        $processor->isCreditVoucher = false;
        $processor->discountDescription .= " ({$promoCode['percent_off']}%)";
        // percent off
        $processor->amountOff = NULL;
        $processor->percentOff = $promoCode['percent_off'];
      }

      if (!$processor->isCreditVoucher) {
        // ignore pre 2021 % off discounts
        if (substr(CRM_Utils_Date::processDate(CRM_Utils_Array::value('created', $processor->attendee)), 0, 4) < "2021") {
          return;
        }
      }
    } else {
      // we have a discount activity which will tell us information
      \CRM_Core_Error::debug_var("existing", $existingActivity);
      $processor->isCreditVoucher = ($existingActivity[DISCOUNT_REASON_FIELD] < 1000);
      \CRM_Core_Error::debug_log_message("set isCreditVoucher to $processor->isCreditVoucher");
      $processor->amountOff = $existingActivity[CREDIT_AMOUNT_FIELD];
      $processor->percentOff = $existingActivity[PERCENT_OFF_FIELD];
      \CRM_Core_Error::debug_var("processor", $processor);
    }

    self::createOrUpdateDiscountActivity($processor, $existingActivity);

    if ($processor->isCreditVoucher and $processor->valueUsed < $processor->amountOff) {
      // make a new voucher with the balance remaining
      $newRemainderVoucherParams = [
        'activity_type_id' => 89,
        'target_id' => $processor->contactId,
        CREDIT_AMOUNT_FIELD => ($processor->amountOff - $processor->valueUsed), 
        ORIGIN_ID_FIELD => $processor->discountActivity[ORIGIN_ID_FIELD],
        ORIGINAL_VALUE_FIELD => $processor->discountActivity[ORIGINAL_VALUE_FIELD],
        DISCOUNT_REASON_FIELD => $processor->discountActivity[DISCOUNT_REASON_FIELD],
      ];
      $remainder = _eventbrite_civicrmapi('Activity', 'create', $newRemainderVoucherParams);
    }
  }

  public function handleDataLoaded($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleDataLoaded");
    $processor = $symfonyEvent->getSubject();

    if (!$processor instanceof \CRM_Eventbrite_WebhookProcessor_Attendee) {
      return;
    }

    $eb = CRM_Eventbrite_EventbriteApi::singleton();
    $ticketClassId = $processor->attendee['ticket_class_id'];
    $path = "events/{$processor->attendee['event_id']}/ticket_classes/$ticketClassId";
    $processor->ticketClass = $eb->request($path);
    \CRM_Core_Error::debug_var("ticket class is ", $processor->ticketClass);

    $processor->ticketPrice = $processor->ticketClass['cost']['major_value'];
    \CRM_Core_Error::debug_var("original ticket price", $processor->ticketPrice);

    self::processPromoCode($processor);
    self::processManualRegistration($processor);
  }

  public function handleExistingContactsMatched($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleExistingContactsMatched");
    $processor = $symfonyEvent->getSubject();
  
  }

  public function handleTicketTypeRoleAssigned($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleTicketTypeRoleAssigned");
    $processor = $symfonyEvent->getSubject();
    // student role always for EB events
    $processor->currentRoleId = STUDENT_ROLE;
  }

  public function handleAttendeeProfileAssigned($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleAttendeeProfileAssigned");
    $processor = $symfonyEvent->getSubject();
    // don't store addresses we don't need them
    unset($processor->attendeeProfile['address']);
  }

  public function handleParticipantParamsAssigned($symfonyEvent) {
    \CRM_Core_Error::debug_log_message("in handleParticipantParamsAssigned");
    $processor = $symfonyEvent->getSubject();
    if ($processor->isCreditVoucher) {
      $processor->participantParams['participant_fee_level'] = $processor->ticketClass['cost']['display'];
      $processor->participantParams['participant_fee_amount'] = $processor->ticketClass['cost']['major_value'];
    }
  }
}
